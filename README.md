# Zigbee-Gateways

## Zigbee网关集合
| 图片 | 设备 | 描述 | 工作模式 | 教程 |
| ---- | ---- | ---- | ---- | ---- |
| <img src="img/Zigbee-Gateway-USB.jpg" alt="USB-gateway" width="200"/> | [Zigbee USB Gateway](https://www.aliexpress.us/item/3256803441836847.html) | CC2652P2 | USB | [教程](wiki/Zigbee-Gateway-USB.md) |
| <img src="img/Zigbee-Gateway-LAN.jpg" alt="LAN-gateway" width="200"/> | [Zigbee LAN Gateway](https://www.aliexpress.us/item/3256804554006317.html) | CC2652P2 & LAN Modbus | USB/LAN| [教程](wiki/Zigbee-Gateway-LAN.md) |
| <img src="img/Zigbee-Gateway-LAN-PRO.jpg" alt="LAN or POE-gateway" width="200"/> | [Zigbee LAN Gateway PRO](https://www.aliexpress.us/item/3256804557892073.html)<br>[Zigbee POE Gateway PRO](https://www.aliexpress.us/item/3256804675805140.html) | CC2652P2 & ESP32 Module | USB/LAN/POE | [教程](wiki/Zigbee-Gateway-LAN-PRO.md) |